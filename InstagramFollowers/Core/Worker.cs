﻿using InstagramFollowers.Core.Common;
using InstagramFollowers.Core.Interfaces;
using InstagramFollowers.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace InstagramFollowers.Core
{
    public class Worker
    {
        private List<string> _names;
        private IFollowers _followers;
        private MainAccount _mainAccount;
        private List<string> _closedAcces;
        private int _countOfSubscribers;

        public Worker(IFollowers followers)
        {
            Logger.WriteWithDate("Start", ConsoleColor.Cyan);

            _followers = followers;
            _closedAcces = new List<string>();

            InitNames();
            InitMainAccount();

            _followers.OnAccountClosed += (userLink) =>
            {
                _closedAcces.Add(userLink);
                Logger.WriteWithDate($"AccountClosed -> {userLink}", ConsoleColor.Red);
            };
        }

        private void InitMainAccount()
        {
            _mainAccount = new MainAccount();
            
            Console.Write("Введите логин: ");
            _mainAccount.Login = Console.ReadLine();

            Console.Write("Введите пароль: ");
            _mainAccount.Password = Console.ReadLine();

            _mainAccount.UserLink = $"https://www.instagram.com/{_mainAccount.Login}/";
        }

        private void InitNames()
        {
            Console.Write("Введите путь к файлу: ");
            var pathToNames = Console.ReadLine();

            if (!File.Exists(pathToNames))
                throw new Exception("Файла не сущесвует");

            _names = File.ReadAllLines(pathToNames).ToList();
            Logger.WriteWithDateAndStartWitnN($"Names: {_names.Count()}\r\n", ConsoleColor.Yellow);
        }

        public void Work()
        {
            new Thread(() =>
            {
                try
                {
                    var rnd = new Random();
                    var result = _followers.Auth(_mainAccount);
                    if (!result)
                        throw new Exception("Провертье правильность введеных данных");

                    _countOfSubscribers = _followers.GetCountOfSubscribers(_mainAccount.UserLink);

                    var around = 1;

                    while (true)
                    {
                        Logger.WriteWithDateAndStartWitnN($"Круг: {around}\r\n", ConsoleColor.Yellow);

                        OnAllUsers(_followers.Follow); // Подписываемся

                        Waiting(around);

                        OnAllUsers(_followers.UnFollow); // Отписываемся

                        around++;
                    }
                }
                catch (Exception ex)
                {
                    Logger.WriteWithDate($"Work -> {ex.Message}", ConsoleColor.Red);
                }
            }).Start();
        }

        public void OnAllUsers(Func<string, bool> action)
        {
            var rnd = new Random();
            for (int i = 0; i < _names.Count(); i++)
            {
                try
                {
                    var userLink = _names[i];
                    if (_closedAcces.Contains(userLink))
                        throw new Exception($"Закрытый аккаунт -> {userLink}");


                    var result = action?.Invoke(userLink);
                    if (!result.Value)
                        throw new Exception($"Не удалось выполнить действие -> {userLink}");

                    Thread.Sleep(TimeSpan.FromSeconds(rnd.Next(30, 60)));
                }
                catch (Exception ex)
                {
                    Logger.WriteWithDate(ex.Message, ConsoleColor.Red);
                }
            }
        }

        public void Waiting(int around)
        {
            TimeSpan minutes;

            if (around % 8 == 0)
                minutes = TimeSpan.FromMinutes(120);
            else if (around % 4 == 0)
                minutes = TimeSpan.FromMinutes(60);
            else
                minutes = TimeSpan.FromMinutes(20);

            Logger.WriteWithDateAndStartWitnN($"Ждем {minutes.TotalMinutes} минут...\r\n", ConsoleColor.Yellow);

            Thread.Sleep(minutes);
        }

        public void Statistics()
        {
            var currentCntOfSubscribers = _followers.GetCountOfSubscribers(_mainAccount.UserLink);

            var sb = new StringBuilder();
            sb.AppendLine($"Было: {_countOfSubscribers, 5}");
            sb.AppendLine($"Сейчас: {currentCntOfSubscribers}");
            sb.AppendLine($"Профит: {currentCntOfSubscribers - _countOfSubscribers}");

            Logger.WriteResult(sb.ToString(), ConsoleColor.Yellow);
        }
    }
}

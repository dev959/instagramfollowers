﻿using InstagramFollowers.Core.Models;
using System.IO;
using System.Net;

namespace InstagramFollowers.Core.Common
{
    public static class RequestHelper
    {
        public static string UserAgent { get; private set; }

        static RequestHelper()
        {
            UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36";
        }

        public static RequestContext GetRequestContext()
        {
            return new RequestContext
            {
                Cookies = new CookieContainer(),
                UserAgent = UserAgent
            };
        }

        public static HttpWebRequest CreateRequest(this RequestContext requestContext, string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = requestContext.UserAgent;
            request.CookieContainer = requestContext.Cookies;
            request.KeepAlive = true;

            return request;
        }

        public static string Get(this HttpWebRequest request, RequestContext requestContext)
        {
            string html = string.Empty;
            request.Method = "GET";

            using (var response = request.GetResponse())
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                html = sr.ReadToEnd();
            }

            requestContext.Cookies = request.CookieContainer;

            return html;
        }

        public static void SetDefaultParameters(this HttpWebRequest request, string csrftoken, string hash)
        {
            request.Headers.Add(HttpRequestHeader.Accept, "*/*");
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate, br");
            request.Headers.Add(HttpRequestHeader.AcceptLanguage, "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
            request.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
            request.Headers.Add(HttpRequestHeader.Referer, "https://www.instagram.com/");
            request.Headers.Add("sec-fetch-dest", "empty");
            request.Headers.Add("sec-fetch-mode", "cors");
            request.Headers.Add("sec-fetch-site", "same-origin");
            request.Headers.Add("x-csrftoken", csrftoken);
            request.Headers.Add("X-IG-App-ID", "936619743392459");
            request.Headers.Add("X-Instagram-AJAX", hash);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InstagramFollowers.Core.Common
{
    public static class Logger
    {
        public static bool IsErrorPoint;

        private static object _locker;
        static Logger() => _locker = new object();

        public static void WriteResult(string msg, ConsoleColor consoleColor)
        {
            lock (_locker)
            {
                Console.ForegroundColor = consoleColor;

                if (IsErrorPoint)
                    Console.Write("*");
                else
                    Console.WriteLine(msg);

                Console.ResetColor();
            }
        }

        public static void WriteWithDate(string msg, ConsoleColor consoleColor)
        {
            lock (_locker)
            {
                Console.ForegroundColor = consoleColor;

                if (IsErrorPoint)
                    Console.Write("*");
                else
                    Console.WriteLine($"[{DateTime.Now.ToLongTimeString()}] {msg}");

                Console.ResetColor();
            }
        }

        public static void WriteWithDateAndStartWitnN(string msg, ConsoleColor consoleColor)
        {
            lock (_locker)
            {
                Console.ForegroundColor = consoleColor;

                if (IsErrorPoint)
                    Console.Write("*");
                else
                    Console.WriteLine($"\r\n[{DateTime.Now.ToLongTimeString()}] {msg}");

                Console.ResetColor();
            }
        }
    }
}

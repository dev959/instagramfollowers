﻿using InstagramFollowers.Core;
using InstagramFollowers.Core.Common;
using InstagramFollowers.Core.Interfaces;
using InstagramFollowers.Core.Models;
using System;
using System.Text;
using System.Threading;

namespace InstagramFollowers
{
    class Program
    {
        static void Main(string[] args)
        {
            var worker = new Worker(new Followers());
            worker.Work();

            while (true)
            {
                var str = Console.ReadLine();
                if (str == "statistics")
                    worker.Statistics();
            }
        }
    }
}

﻿using InstagramFollowers.Core.Common;
using InstagramFollowers.Core.Interfaces;
using InstagramFollowers.Core.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace InstagramFollowers.Core
{
    public class Followers : IFollowers
    {
        public event Action<string> OnAccountClosed;

        private RequestContext _requestContext;

        public Followers()
        {
            _requestContext = RequestHelper.GetRequestContext();
        }

        public bool Auth(MainAccount account)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    var html = string.Empty;

                    #region Получаем токен

                    var request = _requestContext.CreateRequest("https://www.instagram.com/");
                    var defPar = GetDefaultParameters(request);
                    if (string.IsNullOrEmpty(defPar.Csrftoken) || string.IsNullOrEmpty(defPar.Hash))
                        throw new Exception("Не удалось получить токен");

                    #endregion

                    #region Авторизация

                    request = _requestContext.CreateRequest("https://www.instagram.com/accounts/login/ajax/");
                    request.Method = "POST";

                    request.SetDefaultParameters(defPar.Csrftoken, defPar.Hash);

                    var dataStr = $"username={account.Login}&enc_password=&password={account.Password}&queryParams={{}}&optIntoOneTap=false";

                    var buffer = Encoding.UTF8.GetBytes(dataStr);
                    request.ContentLength = buffer.Length;

                    using (var sw = request.GetRequestStream())
                        sw.Write(buffer, 0, buffer.Length);

                    using (var resp = request.GetResponse())
                    using (var sr = new StreamReader(resp.GetResponseStream()))
                        html = sr.ReadToEnd();

                    #endregion

                    #region Парсим результат

                    dynamic jObj = JObject.Parse(html);

                    if (!(bool)jObj.authenticated)
                        throw new Exception("Не удалось авторизоваться");

                    #endregion

                    Logger.WriteWithDate($"{account.Login} - вы успешно авторизовались!", ConsoleColor.Cyan);

                    return true;
                }
                catch (Exception ex)
                {
                    Logger.WriteWithDate($"Auth -> {ex.Message}", ConsoleColor.Red);
                }
            }

            return false;
        }

        public bool Follow(string userLink)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    var html = string.Empty;

                    #region Получаем userId и токен

                    var request = _requestContext.CreateRequest(userLink);
                    var defPar = GetDefaultParameters(request, true);

                    if (string.IsNullOrEmpty(defPar.UserId) || string.IsNullOrEmpty(defPar.Csrftoken))
                        throw new Exception("Не удалось получить userId или токен");

                    #endregion

                    #region Подписываемся на пользователя

                    request = _requestContext.CreateRequest($"https://www.instagram.com/web/friendships/{defPar.UserId}/follow/");
                    request.Method = "POST";
                    request.SetDefaultParameters(defPar.Csrftoken, defPar.Hash);
                    request.ContentLength = 0;

                    using (var resp = request.GetResponse())
                    using (var sr = new StreamReader(resp.GetResponseStream()))
                        html = sr.ReadToEnd();

                    #endregion

                    #region Парсим результат

                    dynamic jObj = JObject.Parse(html);

                    string result = jObj.result;

                    if (result == "requested")
                        throw new Exception("AccountClosed");

                    else if (result != "following")
                        throw new Exception("Не удалось подписаться");

                    #endregion

                    Logger.WriteWithDate($"Подписался -> {userLink}", ConsoleColor.Green);

                    return true;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Equals("AccountClosed"))
                    {
                        OnAccountClosed?.Invoke(userLink);
                        return false;
                    }

                    Logger.WriteWithDate($"Follow -> {ex.Message}", ConsoleColor.Red);
                }
            }

            return false;
        }

        public bool UnFollow(string userLink)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    var html = string.Empty;

                    #region Получаем userId и токен

                    var request = _requestContext.CreateRequest(userLink);
                    var defPar = GetDefaultParameters(request, true);

                    if (string.IsNullOrEmpty(defPar.UserId) || string.IsNullOrEmpty(defPar.Csrftoken))
                        throw new Exception("Не удалось получить userId или токен");

                    #endregion

                    #region Подписываемся на пользователя

                    request = _requestContext.CreateRequest($"https://www.instagram.com/web/friendships/{defPar.UserId}/unfollow/");
                    request.Method = "POST";
                    request.SetDefaultParameters(defPar.Csrftoken, defPar.Hash);
                    request.ContentLength = 0;

                    using (var resp = request.GetResponse())
                    using (var sr = new StreamReader(resp.GetResponseStream()))
                        html = sr.ReadToEnd();

                    #endregion

                    #region Парсим результат

                    dynamic jObj = JObject.Parse(html);

                    if ((string)jObj.status != "ok")
                        throw new Exception("Не удалось отписаться");

                    #endregion

                    Logger.WriteWithDate($"Отписался -> {userLink}", ConsoleColor.DarkYellow);

                    return true;
                }
                catch (Exception ex)
                {
                    Logger.WriteWithDate($"UnFollow -> {ex.Message}", ConsoleColor.Red);
                }
            }

            return false;
        }

        public int GetCountOfSubscribers(string userLink)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    var request = _requestContext.CreateRequest(userLink);
                    string html = request.Get(_requestContext);

                    string currentCountOfSubscribersStr = Regex.Match(html, "\"edge_followed_by\":{\"count\":(.*?)},").Groups[1].Value;
                    if (string.IsNullOrEmpty(currentCountOfSubscribersStr) || !int.TryParse(currentCountOfSubscribersStr, out int currentCountOfSubscribers))
                        throw new Exception("Не удалось спарсить кол-во подписчиков");

                    return currentCountOfSubscribers;
                }
                catch (Exception ex)
                {
                    Logger.WriteWithDate($"GetCountOfSubscribers -> {ex.Message}", ConsoleColor.Red);
                }
            }

            return 0;
        }

        private static string GetTokenFromCookies(CookieCollection cookies)
        {
            foreach (Cookie cookie in cookies)
            {
                if (cookie.Name.Contains("csrftoken"))
                    return cookie.Value;
            }

            return null;
        }

        private static string GetRolloutHashFromHtml(string html)
        {
            return Regex.Match(html, "\"rollout_hash\":\"(.*?)\",").Groups[1].Value;
        }

        private DefaultParameters GetDefaultParameters(HttpWebRequest request, bool isUserId = false)
        {
            request.Method = "GET";

            using (var response = (HttpWebResponse)request.GetResponse())
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                var html = sr.ReadToEnd();
                var hash = GetRolloutHashFromHtml(html);
                var csrftoken = GetTokenFromCookies(response.Cookies);
                var defPar = new DefaultParameters
                {
                    Csrftoken = csrftoken,
                    Hash = hash,
                };

                if (isUserId)
                    defPar.UserId = Regex.Match(html, "\"logging_page_id\":\"profilePage_(.*?)\",").Groups[1].Value;

                return defPar;
            }
        }
    }
}

﻿using System.Net;

namespace InstagramFollowers.Core.Models
{
    public class RequestContext
    {
        public string UserAgent { get; set; }
        public CookieContainer Cookies { get; set; }
    }
}

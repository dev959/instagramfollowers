﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InstagramFollowers.Core.Models
{
    public class DefaultParameters
    {
        public string Hash { get; set; }
        public string Html { get; set; }
        public string Csrftoken { get; set; }
        public string UserId { get; set; }
    }
}

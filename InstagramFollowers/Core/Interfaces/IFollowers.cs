﻿using InstagramFollowers.Core.Models;
using System;

namespace InstagramFollowers.Core.Interfaces
{
    public interface IFollowers
    {
        bool Auth(MainAccount account);
        bool Follow(string userLink);
        bool UnFollow(string userLink);
        int GetCountOfSubscribers(string userLink);

        event Action<string> OnAccountClosed;
    }
}

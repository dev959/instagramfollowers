﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace InstagramFollowers.Core.Models
{
    public class MainAccount
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string UserLink { get; set; }
    }
}
